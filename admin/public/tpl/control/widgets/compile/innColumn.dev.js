(function($){
    return {
        compile:function (uicPage,entity,htmlParent) {
            var me = this;

            var div = new $.UIC_HtmlElement("div");
            htmlParent.add(div);

            //=== 样式
            var g = new $.StyleGen(entity);
            var style = g.gen();

            var cls = $.toString(me._genClass(uicPage,entity));
            if( style.cls ) cls += " " + style.cls;

            if( cls ) div.setProp("class",cls);
            if( style.style ) div.setProp("style",style.style);

            if( entity.value.title ) div.add(entity.value.title);

        },
        _genClass:function (uicPage,entity) {
            return "column";
        }
    }
})(eui)