<div>
    <div class="slide-cg" u-dom="slideDom"
         u-mouseover="onMouseOver"
         u-mouseleave="onMouseLeave">
        <div class="screen">
            <ul>
                <li u-each="pics" u-item="pic"
                    u-index="index"
                    style="display:${index==curindex?'block':'none'}">
                    <a href="javascript:;">
                        <div class="image height-full">
                            <img src="${pic.url}" class="cover-image"/>
                        </div>

                    </a>
                </li>
            </ul>
            <ol>
                <li u-each="pics" u-index="index">
                    <i class="fa fa-circle ${index==curindex?current:''}"></i>
                </li>
            </ol>
        </div>
    </div>

</div>