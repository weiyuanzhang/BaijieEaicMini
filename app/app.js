var $ = require('eui-mini.min.js');
var cfg = require("eaicconfig.js");
var dist = require("dist.js");

App({
    onLaunch: function (obj) {

        var me = this;
        
        me.globalData.eui = $;
        me.globalData.dist = dist.init($);

        $.setConfig( cfg );
    },
    onShow: function (obj) {
        var me = this;
    },
    onHide: function () {
        var me = this;
    },
    onError: function (err) {
        var me = this;
    },
    onPageNotFound: function (obj) {
        var me = this;
    },
    getLoginUser:function (fnOK,fnFail) {
        var me = this;
        if( me.globalData.loginUser && me.globalData.loginUser.id ){
            fnOK(me.globalData.loginUser);
        }else{
            $.getSessionKey()
                .then(function (sid) {
                    if( sid ){
                        $.api($.getConfig("apiLoadCur"),{})
                            .then(function (oUser) {
                                me.globalData.loginUser = oUser;
                                fnOK(me.globalData.loginUser);
                            },function (err) {
                                if( $.isFunction(fnFail)) fnFail(err)
                            })
                    }else{
                        fnOK({});
                    }
                },function (err) {
                    if( $.isFunction(fnFail)) fnFail(err)
                })
        }
    },
    "globalData": {
        "eui": null,
        "dist":null,//分销
        "loginUser":null,
        "haibao": ""
    }
});
