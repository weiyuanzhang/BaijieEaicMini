eui.app({
    data:{
        dialog:{},
        active:"is-active"
    },
    onOK:function (e) {
        var me = this;
        var fn = me.data.dialog.fnOK;
        this.setData({
            dialog:{type:"",fnOK:null,fnCancel:null},
            active:""
        });
        if( eui.isFunction(fn) ) fn();
    },
    onCancel:function (e) {
        var me = this;
        var fn = me.data.dialog.fnOK;
        this.setData({
            dialog:{type:"",fnOK:null,fnCancel:null},
            active:""
        });
        if( eui.isFunction(fn) ) fn();
    }
});