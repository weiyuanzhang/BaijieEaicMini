var $ = getApp().globalData.eui;
$.page({
    data: {
        "addrid": 0,
        "recvaddr": {},
        "product": {},
        "productid": 0
    },
    onLoad: function (options) {

        var me = this;



        me.setData({
            productid: $.toInt(options.id)
        });

        $.api("mall.Product.Load", {
            id: me.data.productid
        })
            .then(function (product) {
                product.pinprice = $.toFloat(product.pinprice);
                me.setData({
                    product: product
                });
                $.api("mall.RecvAddress.LoadDefault", {}).then(function (value) {
                    if (value && $.isObject(value)) {
                        me.setData({
                            "addrid": value.id,
                            "recvaddr": value
                        });
                    }
                });
            });
    }
});