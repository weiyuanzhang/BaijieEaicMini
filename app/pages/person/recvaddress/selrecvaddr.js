var $ = getApp().globalData.eui;
$.page({
	data: {
		"recvaddress": []
	},
	onLoad: function (options) {

		var me = this;

		me.loadPage()

	},

	onSelAddr: function (e) {

		var me = this;

		var index = $.toInt(e.currentTarget.dataset.index);
		var addr = me.data.recvaddress[index];
		me.notify("SelRecvAddress", [addr]);
		$.back();

	},
	onAddRecvAddr: function (e) {

		var me = this;

		$.go("/pages/person/recvaddress/edit");

	},
	notifyAddRecvAddress:function(e){
		var me = this;
		var addr = e;
		me.notify("SelRecvAddress", [addr]);
		$.back();
	},

	loadPage: function () {

		var me = this;
		$.api("mall.RecvAddress.LoadAll", {})
			.then(function (value) {
				$.each(value, function (v) {
					v.isdefault = $.toInt(v.isdefault);
				});
				me.setData({
					"recvaddress": value
				});
			});
	}

});