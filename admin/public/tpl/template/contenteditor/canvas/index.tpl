<div>

    <div
            class="height-full width-full content-editor"
            u-click="onClick"
            u-view="/template/contenteditor/canvas/container/index"
    u-share="{
    'entities':'entities',
    'paper':'paper',
    'activeEntity':'activeEntity',
    'maxid':'maxid',
    'action.removeEntity':'action.removeEntity',
    'action.focusEntity':'action.focusEntity',
    'action.entityPropChange':'action.entityPropChange'
    }"></div>

</div>