$.app({
	data: {
		file: null,//实际的文件
		preurl:"",//用于预览的url
		config: {}
	},

	onLoad:function(){
		var me = this;
		if( $.isString(me.data.file)){
			me.setData({preurl:me.data.file});
		}
	},
	onFileChange: function (e) {
		var me = this;

		var file = e.src.files[0];

		if( file && file.type.indexOf("image") === 0){
			$.log("选取了一个图片...");
			me.addImage(file);
		}

	},
	addImage:function (file) {
		var me = this;

		me.data.file = file;
		var reader = new FileReader();
		reader.onload = function () {
			var url = this.result;
			$.log("preurl:"+url);
			$.test_uploadfile=file;
			me.setData({
				file:file,
				preurl:url
			});
		};

		reader.readAsDataURL(file);
	},
	onRemove:function (e) {
		var me = this;
		me.setData({
			file:null,
			preurl:""
		});
	}

});