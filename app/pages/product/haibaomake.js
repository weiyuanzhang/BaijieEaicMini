var $ = getApp().globalData.eui;
$.page({

    /**
     * 页面的初始数据
     */
    data: {
        "weburl":""
    },

    /**
     * 生命周期函数--监听页面加载
     */
    onLoad: function (options) {
        var me = this;
        var id = $.toInt(options.id);
        $.getSessionKey()
            .then(function (sid) {
                var weburl=$.getConfig("server_url")+"/haibao?sid="+sid+"&id="+id;
                $.log("后台url:"+weburl);
                me.setData({
                    "weburl":weburl
                });
            })
    },

    /**
     * 生命周期函数--监听页面初次渲染完成
     */
    onReady: function () {

    },

    /**
     * 生命周期函数--监听页面显示
     */
    onShow: function () {

    },

    /**
     * 生命周期函数--监听页面隐藏
     */
    onHide: function () {

    },

    /**
     * 生命周期函数--监听页面卸载
     */
    onUnload: function () {

    },

    /**
     * 页面相关事件处理函数--监听用户下拉动作
     */
    onPullDownRefresh: function () {

    },

    /**
     * 页面上拉触底事件的处理函数
     */
    onReachBottom: function () {

    },

    /**
     * 用户点击右上角分享
     */
    onShareAppMessage: function () {

    },
    bindmessage1:function(e){

        var me = this;

        $.log("收到消息:"+$.toJson(e.detail));

        var fnDownloadImg=function (entities,fnOK,fnFail) {
            var me = this;
            $.log("enter fnDownloadImg");
            var fn=function (entities,index,fnOK,fnFail) {
                $.log("downloading...."+index);
                if( index < entities.length ){
                    var entity = entities[index];
                    if( entity.type==='image'){
                        var url = entity.url;
                        if( !(url.indexOf("http:")===0 || url.indexOf("https:")===0) ){
                            url = $.getConfig("server_url")+url;
                        }
                        wx.getImageInfo({
                            src:url,
                            success:function (res) {
                                entity.$width = res.width;
                                entity.$height=res.height;
                                entity.$path=res.path;
                                fn(entities,index+1,fnOK,fnFail)
                            },
                            fail:function (err) {
                                $.log("fail:"+$.toJson(err));
                            }
                        })
                    }else{
                        fn(entities,index+1,fnOK,fnFail)
                    }
                }else{
                    fnOK(entities);
                }
            };
            fn(entities,0,fnOK,fnFail);
        };
        $.each(e.detail.data,function (detail) {
            if( "content" in detail){
                var content = detail.content;
                $.log("===准备下载图片,content="+content);
                fnDownloadImg($.fromJson(content),function (entities) {
                    $.log("--- after download image");
                    detail.content=$.toJson(entities);
                    getApp().globalData.haibao=detail;
                    $.log("--- go");
                    $.go("/pages/product/haibao")
                },function (err) {

                })
            }
        })
        //$.notify("HaibaoMessage",[e.detail]);
        //$.go("/pages/templ/save?url="+e.detail.data[0].url);

    },

});