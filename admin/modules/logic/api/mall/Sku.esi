function getModel () {
    return es.model["mall.Sku"];
}

function getMeta () {
    return getModel ().Meta;
}

public function Add (var input) {
    var productid = int (input["productid"]);
    var oProduct = es.model["mall.Product"].Load ({ "id": productid }, ["id"]);
    if (oProduct == null) throw "参数错误";

    var choices = (es.Encoder ()).FromJson (input["choices"]);
    if (typeof (choices) != "array") throw "参数错误";

    var newVal = { };
    for (var k: ["sn", "icon", "saleprice", "marketprice", "desc", "shippingcost"]) {
        if (input.Has (k)) {
            newVal[k] = input[k];
        }
    }
    newVal["productid"] = productid;

    var skuid = getModel ().Save (input);

    for (var choice : choices) {
        var propid = int (choice["propid"]);
        var choiceid = int (choice["id"]);

        var obj = es.model["mall.Choice"].Load ({
            "propid": propid,
            "id": choiceid
        }, ["id"]);
        if (obj != null) {
            es.model["mall.SkuChoice"].Save ({
                "skuid": skuid,
                "propid": propid,
                "choiceid": choiceid,
                "productid": productid
            });
        }

    }

    var oPin = {
        "productid": productid,
        "skuid": skuid
    };
    if (input.Has ("usepin") && int (input.usepin)) {
        oPin.usepin = 1;
        oPin.pinprice = float (input.pinprice);
        oPin.pintype = int (input.pintype);
        oPin.pinhour = int (input.pinhour);
        oPin.pincount = int (input.pincount);
        oPin.pinstarttime = "" + es.DateTime ().FromString (input.pinstarttime);
        oPin.pinendtime = "" + es.DateTime ().FromString (input.pinendtime);
    }
    es.model["mall.SkuPin"].Save (oPin);

    var oDist = {
        "productid": productid,
        "skuid": skuid
    };
    if (input.Has ("ratiotype")) {
        oDist.ratiotype = int (input.ratiotype);
    }
    if (input.Has ("distratio1")) {
        oDist.distratio1 = float (input.distratio1);
    }
    if (input.Has ("distratio2")) {
        oDist.distratio2 = float (input.distratio2);
    }
    if (input.Has ("distratio3")) {
        oDist.distratio3 = float (input.distratio3);
    }
    es.model["mall.SkuDist"].Save (oDist);

    return skuid;
}

public function Update (var input) {
    var id = int (input["id"]);
    var skuid = id;

    var oSku = getModel ().Load ({ "id": id }, ["id", "productid"]);
    if (oSku == null) { throw "参数错误"; }
    var productid = int (oSku.productid);

    var newVal = { };
    for (var k: ["sn", "icon", "marketprice", "saleprice","vipprice", "online", "desc", "shippingcost"]) {
        if (input.Has (k)) {
            newVal[k] = input[k];
        }
    }

    getModel ().Update (newVal, { "id": id });

    if (input.Has ("choices")) {
        var choices = (es.Encoder ()).FromJson (input["choices"]);
        if (typeof (choices) != "array") throw "参数错误";

        var mChoice = es.meta["mall.Choice"];
        var mSkuChoice = es.meta["mall.SkuChoice"];
        var OldChoices = es.sql.Create ({
                "t": mSkuChoice,
                "c": mChoice
            }, "{t} t left join {c} c on t.choiceid=c.id")
            .Where (["t.skuid=", id])
            .Select ("c.id", "c.name", "t.propid", "c.url")
            .All ();

        //先删除不存了的
        for (var oldChoice : OldChoices) {
            var found = false;
            for (var choice : choices) {
                if (int (choice.id) == int (oldChoice.id)) {
                    found = true;
                    break;
                }
            }
            if (!found) {
                es.model["mall.SkuChoice"].Remove ({ "skuid": id, "choiceid": oldChoice["id"] });
            }
        }

        //再添加新增的
        for (var choice : choices) {
            var found = false;
            for (var oldChoice : OldChoices) {
                if (int (choice.id) == int (oldChoice.id)) {
                    found = true;
                    break;
                }
            }
            if (!found) {
                es.model["mall.SkuChoice"].Save ({
                    "skuid": id,
                    "propid": choice["propid"],
                    "choiceid": choice["id"],
                    "productid": productid
                });
            }
        }
    }

    var oPin = es.model["mall.SkuPin"].Load ({ "productid": productid, "skuid": skuid }, ["id"]);
    if (oPin == null) {
        es.model["mall.SkuPin"].Save ({
            "productid": productid,
            "skuid": skuid,
            "usepin": int (input.usepin),
            "pinprice": float (input.pinprice),
            "pintype": int (input.pintype),
            "pinhour": int (input.pinhour),
            "pincount": int (input.pincount),
            "pinstarttime": (input.pinstarttime ? input.pinstarttime : "" + es.DateTime ().FromString (input.pinstarttime)),
            "pinendtime": (input.pinendtime?input.pinendtime: "" + es.DateTime ().FromString (input.pinendtime))
        });
    } else {
        es.model["mall.SkuPin"].Update ({

            "usepin": int (input.usepin),
            "pinprice": float (input.pinprice),
            "pintype": int (input.pintype),
            "pinhour": int (input.pinhour),
            "pincount": int (input.pincount),
            "pinstarttime": (input.pinstarttime ? input.pinstarttime : "" + es.DateTime ().FromString (input.pinstarttime)),
            "pinendtime": (input.pinendtime?input.pinendtime: "" + es.DateTime ().FromString (input.pinendtime))
        }, {
            "productid": productid,
            "skuid": skuid
        });
    }

    var oDist = es.model["mall.SkuDist"].Load ({ "productid": productid, "skuid": skuid }, ["id"]);
    if (oDist == null) {
        es.model["mall.SkuDist"].Save ({
            "productid": productid,
            "skuid": skuid,
            "ratiotype": int (input.ratiotype),
            "distratio1": float (input.distratio1),
            "distratio2": float (input.distratio2),
            "distratio3": float (input.distratio3)
        });
    } else {
        es.model["mall.SkuDist"].Update ({
            "ratiotype": int (input.ratiotype),
            "distratio1": float (input.distratio1),
            "distratio2": float (input.distratio2),
            "distratio3": float (input.distratio3)
        }, { "id": oDist.id });
    }
}

public function Remove (var input) {
    var skuid = int (input.id);
    getModel ().Remove ({ "id": skuid });
    es.model["mall.SkuChoice"].Remove ({ "skuid": skuid });
    es.model["mall.SkuPin"].Remove ({ "skuid": skuid });
    es.model["mall.SkuDist"].Remove ({ "skuid": skuid });
}

public function Load (var input) {

    var mSku = getMeta ();
    var mChoice = es.meta["mall.Choice"];
    var mSkuChoice = es.meta["mall.SkuChoice"];
    var mDist = es.meta["mall.SkuDist"];
    var mPin = es.meta["mall.SkuPin"];

    var ret = es.sql.Create ({
            "t": mSku,
            "d": mDist,
            "p": mPin
        }, "{t} t left join {d} d on t.id=d.skuid left join {p} p on t.id=p.skuid")
        .Where ({
            "and": [
                ["t.id=", input.id]
            ]
        })
        .Select ([
            "t.id", "t.sn", "t.icon", "t.marketprice", "t.saleprice","t.vipprice",
            "t.online", "t.desc", "t.shippingcost",
            "d.ratiotype", "d.distratio1", "d.distratio2", "d.distratio3",
            "p.usepin", "p.pinprice", "p.pintype", "p.pinhour", "p.pincount", "p.pinstarttime", "p.pinendtime"
        ])
        .First ();

    var choices = es.sql.Create ({
            "t": mSkuChoice,
            "c": mChoice

        }, "{t} t left join {c} c on t.choiceid=c.id ")
        .Select ("c.id", "c.name", "t.propid", "c.url")
        .Where (["t.skuid=", int (input["id"])], [es.sql.isnull ("c.id").not ()])
        .All ();
    ret["choices"] = (es.Encoder ()).ToJson (choices);
    return ret;
}

public function LoadPin (var input) {
    return es.model["mall.SkuPin"].Load ({ "skuid": input.id }, ["id", "pincount", "pinprice", "pintype", "pinhour", "pinstarttime", "pinendtime", "usepin"]);
}

public function UpdatePin (var input) {
    es.model["mall.SkuPin"].Update (input, { "id": input.id });
}

public function ListPage (var input) {
    var m = getMeta ();
    var mShip = es.meta["mall.ShippingCost"];
    var mPin = es.meta["mall.SkuPin"];
    var mDist = es.meta["mall.SkuDist"];
    var sqlMap = {
        "t": m,
        "sc": mShip,
        "d": mDist,
        "p": mPin
    };
    var sqlText = "{t} t left join {sc} sc on t.shippingcost=sc.id left join {d} d on t.id=d.skuid left join {p} p on t.id=p.skuid";

    var productid = int (input.productid);

    var cond = [];
    cond.Add (["t.productid=", productid]);

    //搜索
    if (input.Has ("search_text") && "" != input["search_text"]) {
        var text = input["search_text"];
        var c = [];
        c.Add (["t.sn%", text]);
        //c.Add(["t.address%",text]);
        c.Add (["t.`desc`%", text]);
        //c.Add(["t.contractname%",text]);
        cond.Add ({ "or": c });
    }

    //条件
    for (var f: ["sn"]) {
        if (input.Has (f)) {
            cond.Add (["t." + f + "=", input[f]]);
        }
    }

    var order = { "desc": "t.createtime" };

    //记录总数
    var count = int (es.sql.Create (sqlMap, sqlText)
        .Where ({ "and": cond })
        .Select (es.sql.count ("*", "c"))
        .Order (order)
        .First () ["c"]);

    //===查询数据
    var pageindex = 0;
    var pagesize = 0;
    var pages = 0;
    var datas = [];
    if (input.Has ("pageindex") && int (input["pagesize"]) > 0) {
        pageindex = int (input["pageindex"]);
        pagesize = int (input["pagesize"]);
    }
    if (pagesize > 0) {
        pages = (count + pagesize - 1) / pagesize;
        datas = es.sql.Create (sqlMap, sqlText)
            .Where ({ "and": cond })
            .Select ([
                "t.icon", "t.sn", "t.id", "t.createtime", "t.saleprice", "t.vipprice","t.desc",
                "p.usepin", "p.pincount", "p.pintype", "p.pinprice",
                "t.marketprice", "t.online", "t.store", "sc.name as shippingcostname"
            ])
            .Order (order)
            .Page (pageindex, pagesize);
        var mChoice = es.meta["mall.Choice"];
        var mSkuChoice = es.meta["mall.SkuChoice"];
        for (var data : datas) {
            var choices = es.sql.Create ({
                    "t": mSkuChoice,
                    "c": mChoice
                }, "{t} t left join {c} c on t.choiceid=c.id")
                .Where (["t.skuid=", data["id"]], [es.sql.isnull ("c.id").not ()])
                .Select ("c.id", "c.name", "t.propid", "c.url")
                .All ();
            data["choices"] = choices;
        }
    }
    return { "totalcount": count, "totalpage": pages, "datas": datas };

}