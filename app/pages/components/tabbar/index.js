var $ = getApp().globalData.eui;

Component({
	/**
	 * 组件的属性列表
	 */
	properties: {
		tabIndex:{
			type:Number,
			value:0
		}
	},

	/**
	 * 组件的初始数据
	 */
	data: {

		tabs:[
			{
				title:"首页",
				icon:"/images/tab-home.png",
				activeIcon:"/images/tab-home-a.png"
			},
			{
				title:"商城",
				icon:"/images/tab-shop.png",
				activeIcon:"/images/tab-shop-a.png"
            },
			{
				title:"购物车",
				icon:"/images/tab-cart.png",
				activeIcon:"/images/tab-cart-a.png"
			},
			{
				title:"我的",
				icon:"/images/tab-my.png",
				activeIcon:"/images/tab-my-a.png"
			}
		],
	},
	options: {
		addGlobalClass: true,
	},
	observers:{

	},

	/**
	 * 组件的方法列表
	 */
	methods: {
		onClickTab:function (e) {
			var me = this;
			var index = $.toInt(e.currentTarget.dataset.index);
			me.setData({
				tabIndex:index
			})
		}
	},
	lifetimes: {
		// 生命周期函数，可以为函数，或一个在methods段中定义的方法名
		attached: function () {
			var me = this;

		},
		detached: function () {
			var me = this;

		},
	},
});
