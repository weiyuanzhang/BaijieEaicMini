// pages/order/deliver.js
var $ = getApp().globalData.eui;
$.page({

	/**
	 * 页面的初始数据
	 */
	data: {
		deliver: null/*{
			name:"",//物流公司
			sn:"",//物流单号
			//node example: {time:"2020-5-28 15:30:41",name:"aaaa",desc:"is desc",active:true,status:""}
			//active:圆点变大 status:可取primary/success/danger
			nodes:[]

		}*/
	},

	/**
	 * 生命周期函数--监听页面加载
	 */
	onLoad: function (options) {
		var me = this;
		var orderid = $.toInt(options.orderid);
		$.api("mall.Order.GetFlow", {"id": orderid})
			.then(function (value) {
				if (value) {
					$.each(value.nodes, function (node) {
						//格式化节点时间
						var dt = $.convertDateFromString(node.time);
						var today = new Date();
						var day = "";
						var hours = "";
						if (dt.getFullYear() == today.getFullYear()) {
							if (dt.getMonth() == today.getMonth()) {
								if (dt.getDate() == today.getDate()) {
									day = "今天";
								} else if (dt.getDate() == today.getDate() - 1) {
									day = "昨天";
								} else {
									day = "" + (dt.getMonth() + 1) + "月" + dt.getDate() + "日";
								}
							} else {
								day = "" + (dt.getMonth() + 1) + "月" + dt.getDate() + "日";
							}
						} else {
							day = "" + dt.getFullYear() + "-" + (dt.getMonth() + 1) + "-" + dt.getDate();
						}
						hours = "" + dt.getHours() + ":" + dt.getMinutes();
						node.hours = hours;
						node.day = day;
					});

				}
				me.setData({"deliver": value})
			});

	},

	/**
	 * 生命周期函数--监听页面初次渲染完成
	 */
	onReady: function () {

	},

	/**
	 * 生命周期函数--监听页面显示
	 */
	onShow: function () {

	},

	/**
	 * 生命周期函数--监听页面隐藏
	 */
	onHide: function () {

	},

	/**
	 * 生命周期函数--监听页面卸载
	 */
	onUnload: function () {

	},

	/**
	 * 页面相关事件处理函数--监听用户下拉动作
	 */
	onPullDownRefresh: function () {

	},

	/**
	 * 页面上拉触底事件的处理函数
	 */
	onReachBottom: function () {

	},

	/**
	 * 用户点击右上角分享
	 */
	onShareAppMessage: function () {

	}
})