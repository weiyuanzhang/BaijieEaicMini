<div>
    <table class="table is-hoverable is-bordered is-striped">
        <thead>
        <th>
            <div class="flex-middle ${curright[this.makeRightNameAll()]?'has-text-danger':''}"
                 u-click="onCheckAll"
                 data-timeout="300">
                <div class="rp-m">
                    <i class="fa fa-${curright[ this.makeRightNameAll()] ? 'check-square':'square-o'}"></i>
                </div>
                <div>
                    模块
                </div>
            </div>

        </th>
        <th>权限</th>
        <th>简介</th>
        </thead>
        <tbody>
        <tr u-each="apis" u-item="item" u-index="index" u-key="${item.name}">
            <td>
                <div class="flex-middle ${curright[this.makeRightNameModule(item.name)]?'has-text-danger':''}"
                     data-name="${item.name}"
                     u-click="onCheckModule"
                     data-timeout="300">
                    <div class="rp-m">
                        <i class="fa fa-${curright[ this.makeRightNameModule(item.name)] ? 'check-square':'square-o'}"></i>
                    </div>
                    <div u-v="apis[${index}].name"></div>
                </div>

            </td>
            <td>
                <div class="is-inline-block rm-l ${curright[this.makeRightName(item.name,groupname)]?'has-text-danger':''}"
                     data-name="${item.name}"
                     data-group="${groupname}"
                     data-timeout="300"
                     u-click="onCheckGroup"
                     u-each="item.group" u-item="groupname" u-key="${groupname}">
                    <div class="flex-middle">
                        <div class="rp-m">
                            <i class="fa fa-${curright[this.makeRightName(item.name,groupname)] ? 'check-square':'square-o'}"></i>
                        </div>
                        <div>${groupname}</div>
                    </div>
                </div>

            </td>
            <td>
                <div u-v="apis[${index}].desc"></div>
            </td>
        </tr>
        </tbody>
    </table>

</div>