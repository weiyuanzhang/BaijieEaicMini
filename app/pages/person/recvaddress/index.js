var $ = getApp().globalData.eui;
$.page({
    data: {
        "recvaddress": [],
        "title": "收货地址"
    },
    onLoad: function (options) {

        var me = this;

        me.loadPage()

    },
    notifyAddRecvAddress: function (e) {

        var me = this;

        me.loadPage();

    },
    notifyUpdateRecvAddress: function (e) {

        var me = this;

        me.loadPage();

    },

    onEdit: function (e) {

        var me = this;

        $.go("/pages/person/recvaddress/edit?id=" + e.currentTarget.dataset.id);

    },

    onAdd: function (e) {

        var me = this;

        $.go("/pages/person/recvaddress/edit", {}, function (child) {
            child.mapEvent(me, {
                "onAddMeta": "notifyAddMeta"
            });
        });

    },
    notifyLogined:function(){
        var me = this;
        me.loadPage();
    },

    loadPage: function () {

        var me = this;
        $.api("mall.RecvAddress.LoadAll", {}).then(function (value) {
            $.each(value, function (v) {
                v.isdefault = $.toInt(v.isdefault);
            });
            me.setData({
                "recvaddress": value
            });
        });
    }

});