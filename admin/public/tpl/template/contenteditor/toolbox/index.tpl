<div>
    <div class="toolbox-widget" u-each="widgets" u-item="widget" u-index="index"
         draggable="true"  data-index="${index}"
         u-dragstart="onWidgetDragStart"
    >
        <div class="icon is-16x16 is-inline-block">
            <img src="${widget.icon}" class="cover-image"/>
        </div>
        <div class="text" u-v="widget.title"></div>

    </div>
</div>