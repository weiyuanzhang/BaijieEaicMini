// pages/components/statusBar/index.js
var $ = getApp().globalData.eui;
Component({
	/**
	 * 组件的属性列表
	 */
    properties: {
        isWhite: {
            type: Boolean,
            value:false
        }
    },

	/**
	 * 组件的初始数据
	 */
	data: {
		statusBarHeight: 0
    },
    options: {
		addGlobalClass: true,
	},

	/**
	 * 组件的方法列表
	 */
	methods: {},
	lifetimes: {
		// 生命周期函数，可以为函数，或一个在methods段中定义的方法名
		attached: function () {
            var me = this;
            
            var height = $.toInt($.getConfig("statusBarHeight"));
            if (height) {
                me.setData({
                    "statusBarHeight" : height
                })
            } else {
                wx.getSystemInfo({
                    success: function (res) {
                        var height = $.toInt(res.statusBarHeight);
                        $.setConfig("statusBarHeight", height);
                        me.setData({
                            "statusBarHeight" : height
                        })
                    }
                });
            }
		},
		detached: function () {
			var me = this;

		},
	},
});
