$.app({
    data:{
        arrDom:null,
        slideDom:null,
        arrDisplay:"none",
        curindex:0,
        test:'aa',
        pics:[]//[{type:"image",url:""},...]
    },
    timer:0,
    onLoad:function () {
        var me = this;

        me.start();
    },
    onUnload:function () {
        var me = this;
        me.stop();
    },
    start:function () {
        var me = this;
        if( !me.timer )
            me.timer=setInterval(function(){
                me.doRight();
            },3000);
    },
    stop:function () {
        var me = this;
        if( me.timer ) {
            clearInterval(me.timer);
            me.timer = 0;
        }
    },
    onMouseOver:function(){
        var me = this;

        me.setData({
            arrDisplay:"block"
        });

        me.stop();
    },
    onMouseLeave:function(){
        var me = this;

        me.setData({
            arrDisplay:"none"
        });

        me.start();
    },
    doRight:function(){
        var me = this;
        var index = me.data.curindex;
        index ++;
        if( index >= me.data.pics.length ) index=0;
        me.setData({
            curindex:index
        });
    },
    doLeft:function(){
        var me = this;
        var index = me.data.curindex;
        index --;
        if( index < 0 ) index=0;
        me.setData({
            curindex:index
        });
    }
});
