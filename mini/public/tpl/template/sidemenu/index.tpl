<div>
	<div u-each="menu" u-item="item" >
		<div>
			<div class="clearfix menurow ${curpath==item.path ? 'active':''} ${item.expand}"
				 u-click="onMenu"
				 data-path="${ item.path }">

				<div class="pull-left menurow-title">
					<img src="/tpl/theme/default/images/${item.icon}"/>
					${item.title}
				</div>

				<div u-if="item.funcs" class="pull-right text-right">
					<i class="fa fa-angle-${item.expand ? 'down' : 'right'}"></i>
				</div>
			</div>

			<div u-if="item.funcs" class="${item.expand ? '' : 'hidden'}" >
				<div u-each="item.funcs" u-item="subitem" >
					<div class="clearfix menurow subrow ${curpath==subitem.path ? 'active':''}"
						 u-click="onMenu"
						  data-path="${subitem.path}">
						<div class="pull-left menurow-title2" >
							<img src="/tpl/theme/default/images/${subitem.icon}"/>
							${subitem.title}
						</div>
						<div class="pull-right">

						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>