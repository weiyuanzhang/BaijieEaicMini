var $ = getApp().globalData.eui;
var biz = require("../../../biz.js");
var Decimal = require("../../../decimal.js");

$.page({
    data: {
        "addrid": 0,
        "recvaddr": {},
        "sku": {
            icon:"",
            saleprice:0
        },
        product:{
            name:""
        },
        skuid:0,
        num:1,
        shipfee:0,
        totalprice:0
    },
    onLoad: function (options) {

        var me = this;
        var skuid=$.toInt(options.id);

        $.api("mall.Sku.Load",{id:skuid})
            .then(function (sku) {
                sku.pintype = $.toInt(sku.pintype);
                sku.pincount = $.toInt(sku.pincount);
                sku.pinprice = $.toFloat(sku.pinprice);
                sku.saleprice = $.toFloat(sku.saleprice);
                sku.shenprice = ""+new Decimal(sku.saleprice).minus(sku.pinprice);
                //$.log("sku="+$.toJson(sku));
                var tool = biz.SkuPriceTool.create(sku,1);
                var shipfee = tool.getShipFee();

                me.setData({
                    sku:sku,
                    shipfee:shipfee,
                    totalprice:$.toFloat(sku.pinprice+shipfee).toFixed(2)
                });

                $.api("mall.Product.Load",{id:sku.productid})
                    .then(function (product) {
                        me.setData({
                            product:product,
                            skuid:skuid
                        });
                    })
            });
        $.api("mall.RecvAddress.LoadDefault", {}).then(function (value) {
            if (value && $.isObject(value)) {
                me.setData({
                    "addrid": value.id,
                    "recvaddr": value
                });
            }
        });

    },
    onSelRecvAddr: function (e) {
        var me = this;
        $.go("/pages/person/recvaddress/selrecvaddr");
    },
    onNumChange:function(e){
        var me = this;
        var num=e.detail.value;

        var tool = biz.SkuPriceTool.create(me.data.sku,num);
        var shipfee = tool.getShipFee();
        var totalprice = new Decimal(me.data.sku.pinprice).times(num).plus(shipfee);
        //$.toFloat(me.data.sku.pinprice*num+shipfee).toFixed(2);
        me.setData({
            num : num,
            shipfee:shipfee,
            totalprice:""+totalprice
        });

    },
    onStartPay: function (e) {
        var me = this;

        if (!me.data.addrid) {
            $.errorBox("错误", "请选择收货地址", function () {
            });
            return;
        }

        $.log("=== addPin,num:"+me.data.num+" skuid:"+me.data.skuid);

        $.api("mall.Order.AddPin", {
            skuid: me.data.skuid,
            addrid: me.data.addrid,
            num:me.data.num
        })
            .then(function (value) {
                //$.alertBox("提示","提交成功");
                $.api("mall.Order.Pay", {
                    orderid: value
                })
                    .then(function (value) {
                        wx.requestPayment({
                            'timeStamp': value.timeStamp,
                            'nonceStr': value.nonceStr,
                            'package': value.package,
                            'signType': 'MD5',
                            'paySign': value.paySign,
                            'success': function (res) {
                                $.alertBox("提示", "支付成功")
                                    .then(function () {
                                        $.back();
                                    })
                            },
                            'fail': function (res) {
                                console.log("支付 res:" + $.toJson(res));
                                //支付 res:{"err_code":"-1","err_desc":"调用支付JSAPI缺少参数: package","errMsg":"requestPayment:fail"}
                                if (res.errMsg.indexOf("fail cancel")) {
                                    $.alertBox("提示", "支付取消");
                                } else {
                                    $.errorBox("错误", "支付失败：" + res.errMsg + " 原因：" + res.err_desc);
                                }

                            },
                            'complete': function (res) {
                                //$.back();
                            }
                        })
                    });
            })

    },
    notifySelRecvAddress: function (addr) {
        var me = this;
        me.setData({
            addrid: addr.id,
            recvaddr: addr
        });
    }
});