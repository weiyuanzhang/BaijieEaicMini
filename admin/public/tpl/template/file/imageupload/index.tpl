<div>
    <div class="imageupload">
        <div class="pre-image">
            <img u-if="preurl" class="cover-image " src="${preurl}"/>
        </div>
        <button class="delete is-small" data-index="${index}" u-click="onRemove"></button>
        <div class="add-button flex-center">
            <div u-unless="preurl" class="flex-middle width-full">
                <div class="flex-tb flex-middle width-full">
                    <div class="flex-center">+</div>
                    <div class="flex-center">添加图片</div>
                </div>
            </div>
            <div class="form-wrap">
                <form>
                    <input u-change="onFileChange" type="file" accept="image/*" />
                </form>
            </div>

        </div>


    </div>



</div>