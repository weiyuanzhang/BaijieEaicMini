$.app({
	data:{
		sheet:{},
		status_str:["已创建","已派单","已接单","已完成","已取消"],
		finishstatus_str:["未知","已完成","未完成"]
	},
	onLoad:function () {
		var me = this;
		var sheet = me.data.sheet;
		sheet.status = $.toInt(sheet.status);
		sheet.userto = $.toInt(sheet.userto);
		sheet.finishstatus = $.toInt(sheet.finishstatus);
		me.setData({sheet:sheet});
	}
});