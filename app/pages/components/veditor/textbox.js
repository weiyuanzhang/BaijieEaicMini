// pages/components/text.js
Component({
    /**
     * 组件的属性列表
     */
    properties: {
        entity: {
            type: Object,
            value: null
        }
    },

    /**
     * 组件的初始数据
     */
    data: {
        nodes:""
    },

    /**
     * 组件的方法列表
     */
    methods: {},
    lifetimes:{
        attached:function () {
            var me = this;
            let data = me.data.entity.value.text;
            me.setData({ nodes:data })
        }
    }
});
