<div>
    <div class="tab">
        <div class="tab-title-wrap">
            <div u-each="tabs" u-item="tab" u-index="index"
                 u-v="tab.title"
                 u-click="onClick" data-index="${index}"
                 class="tab-title ${activeIndex==index ? 'active':''}">

            </div>
        </div>
        <div class="tab-body-wrap">
            <div u-each="tabs" u-item="tab" u-index="index"
                 u-view="${tab.view}" u-ecss="${tab.ecss}" u-app="${tab.app}"
                 class="tab-content ${activeIndex==index ? '':'hidden'}">

            </div>
        </div>

    </div>
</div>