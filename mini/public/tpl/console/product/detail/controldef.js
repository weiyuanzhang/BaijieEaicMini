(function () {
	return [
		{
			key:"slidebox",
			icon:"/tpl/console/product/detail/controls/slidebox/images/slidebox.png",
			isNormal:true,
			style:"",
			pageUrl:"",
			hint:"轮播",
			onCreate:function (entity) {
				entity.value.images=[];

				entity.value.border.lock = 0;
				entity.value.border.width.all = 1;
				entity.value.border.style.all=1;
				entity.value.border.color.all="#e0e0e0";

				entity.value.size.width.type=2;
				entity.value.size.width.value=320;
				entity.value.size.height.type=4;
				entity.value.size.height.value=120;
			}
		},

		{
			key:"metalarea",
			icon:"/tpl/console/product/detail/controls/metalarea/images/metalarea.png",
			isNormal:true,
			style:"",
			pageUrl:"",
			hint:"金刚区",
			onCreate:function (entity) {
				entity.value.linebtncount=4;//每行按钮个数
				entity.value.entries=[];//功能入口 [{icon:"",title:"",type:0,url:""},...]
				//type=0 打开预定义页面
				//type=1 打开指定页面
				//type=2 打开指定网址
				//

				entity.value.border.lock = 0;
				entity.value.border.width.all = 1;
				entity.value.border.style.all=1;
				entity.value.border.color.all="#e0e0e0";

				entity.value.size.width.type=2;
				entity.value.size.width.value=320;
				entity.value.size.height.type=4;
				entity.value.size.height.value=120;
			}
		},
		{
			key:"scrollview",
			icon:"/tpl/console/product/detail/controls/scrollview/images/scrollview.png",
			isNormal:true,
			isContainer:true,
			style:"",
			pageUrl:"",
			hint:"滚动容器",
			onCreate:function (entity) {
				entity.value.border.lock = 0;
				entity.value.border.width.all = 1;
				entity.value.border.style.all=1;
				entity.value.border.color.all="#e0e0e0";

				entity.value.size.width.type=2;
				entity.value.size.width.value=320;
				//entity.value.size.height.type=4;
				//entity.value.size.height.value=120;
			}
		},
		{
			key:"listgroup",
			icon:"/tpl/console/product/detail/controls/listgroup/images/listgroup.png",
			isNormal:true,
			isContainer:true,
			style:"",
			pageUrl:"",
			hint:"列表组",
			onCreate:function (entity) {
				entity.value.border.lock = 0;
				entity.value.border.width.all = 1;
				entity.value.border.style.all=1;
				entity.value.border.color.all="#e0e0e0";

				entity.value.size.width.type=2;
				entity.value.size.width.value=320;
				//entity.value.size.height.type=4;
				//entity.value.size.height.value=120;
			}
		},
		{
			key:"listitem",
			icon:"/tpl/console/product/detail/controls/listitem/images/listitem.png",
			isNormal:true,
			isContainer:false,
			style:"",
			pageUrl:"",
			hint:"列表项",
			onCreate:function (entity) {
				entity.value.title="列表项";
				entity.value.icon="";
				entity.value.type=0;
				entity.value.pageid=0;
				entity.value.url="";
				entity.value.params="";

				entity.value.border.lock = 0;
				entity.value.border.width.all = 1;
				entity.value.border.style.all=1;
				entity.value.border.color.all="#e0e0e0";

				entity.value.size.width.type=2;
				entity.value.size.width.value=320;
				//entity.value.size.height.type=4;
				//entity.value.size.height.value=120;
			}
		},
		{
			key:"loginusericon",
			icon:"/tpl/console/product/detail/controls/loginusericon/images/loginusericon.png",
			isNormal:true,
			isContainer:false,
			style:"",
			pageUrl:"",
			hint:"登录用户头像",
			onCreate:function (entity) {
				entity.value.border.lock = 0;
				entity.value.border.width.all = 1;
				entity.value.border.style.all=1;
				entity.value.border.color.all="#e0e0e0";

				entity.value.size.width.type=4;
				entity.value.size.width.value=32;
				entity.value.size.height.type=4;
				entity.value.size.height.value=32;
			}
		},
		{
			key:"loginuseralias",
			icon:"/tpl/console/product/detail/controls/loginuseralias/images/loginuseralias.png",
			isNormal:true,
			isContainer:false,
			style:"",
			pageUrl:"",
			hint:"登录用户昵称",
			onCreate:function (entity) {
				entity.value.border.lock = 0;
				entity.value.border.width.all = 1;
				entity.value.border.style.all=1;
				entity.value.border.color.all="#e0e0e0";

				entity.value.size.width.type=2;
				entity.value.size.width.value=32;
				entity.value.size.height.type=4;
				entity.value.size.height.value=32;
			}
		},
		{
			key:"memberfeebutton",
			icon:"/tpl/console/product/detail/controls/memberfeebutton/images/memberfeebutton.png",
			isNormal:true,
			isContainer:true,
			style:"",
			pageUrl:"/memberpurchase",
			hint:"续费按钮",
			onCreate:function (entity) {
				entity.value.feetype=0;//0:请选择 1:月卡 2:年卡

				entity.value.border.lock = 0;
				entity.value.border.width.all = 1;
				entity.value.border.style.all=1;
				entity.value.border.color.all="#e0e0e0";

				entity.value.size.width.type=2;
				entity.value.size.width.value=32;
				entity.value.size.height.type=4;
				entity.value.size.height.value=32;
			}
		},
		{
			key:"openpage",
			icon:"/tpl/console/product/detail/controls/openpage/images/openpage.png",
			isNormal:true,
			isContainer:true,
			style:"",
			pageUrl:"",
			hint:"打开页面",
			onCreate:function (entity) {
				entity.value.url="";//页面路径

				entity.value.border.lock = 0;
				entity.value.border.width.all = 1;
				entity.value.border.style.all=1;
				entity.value.border.color.all="#e0e0e0";

				entity.value.size.width.type=2;
				entity.value.size.width.value=32;
				entity.value.size.height.type=4;
				entity.value.size.height.value=32;
			}
		}
	]
})();