// pages/components/children.js
var $ = getApp().globalData.eui;
Component({
    /**
     * 组件的属性列表
     */
    properties: {
        children: {
            type: Array,
            value: []
        }
    },

    /**
     * 组件的初始数据
     */
    data: {},

    /**
     * 组件的方法列表
     */
    methods: {},
    lifetimes: {
        attached: function() {
            var me = this;
            //$.log("=== children=" + $.toJson(me.data.children));
        },
        detached: function() {
            // 在组件实例被从页面节点树移除时执行
        },
    }
})
