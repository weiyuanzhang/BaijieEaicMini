<div>
    <div class="titlebar clearfix ">
        <div class="bar-left ${winbar.canback ? '':'is-hidden'}" u-click="onBack">
            <i class="fa fa-chevron-left"></i>
        </div>
        <div class="bar-right"></div>
        <div class="bar-center">${winbar.title}</div>
    </div>
</div>